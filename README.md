# README #

### This is a fortune-file containing evil computer quotes. ###

### Install ###

* Copy "waue"-file to `[path]/[to]/[fortune]/share/games/fortunes/waue-dir`
* create "waue.dat" in that same path in CMD: `strfile waue waue.dat`
* repeat that last command after each update of the "waue"-file to create a new "waue.dat"-file
* access via CMD: `fortune waue`
